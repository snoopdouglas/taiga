#!/usr/bin/env python3

# taiga: incremental build tool

version = '0.5.0'

# Copyright (c) 2019 Douglas Thompson

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys
import re
import json
import yaml
import copy
import subprocess
import time
import asyncio
import argparse
import multiprocessing
import hashlib

defaults = {
  'config_path': 'taiga.yml',
  'jobs_max':    multiprocessing.cpu_count(),
}


# --- utils

def msg(*args, **kwargs):
  print("=== " + ('\n'.join(map(lambda x: str(x).strip(), args))))
  if 'exit' in kwargs:
    exit(kwargs['exit'])

def listify(v):
  if isinstance(v, list):
    return v
  return [v]


# --- cli

parser = argparse.ArgumentParser(description="incremental build tool")

parser.add_argument('-f', '--file',
  default=defaults['config_path'],
  help="read config from FILE (default: {})".format(defaults['config_path']),
)
parser.add_argument('-t', '--target', help='set build target (default: all dependencies)')
parser.add_argument('-j', '--jobs',
  default=defaults['jobs_max'],
  type=int,
  help="maximum number of parallel jobs (default: {})".format(defaults['jobs_max']),
)
parser.add_argument('-d', '--dump',
  metavar="FILE",
  help="dump dependency mapping to FILE",
)
parser.add_argument('-D', '--define',
  nargs='+',
  metavar="VAR=VALUE",
  help="set build variables",
)
parser.add_argument('-v', '--verbose', help="verbose output", action='store_true')
parser.add_argument('-V', '--version', help="print version and exit", action='store_true')

args = parser.parse_args()

if args.version:
  print(version)
  exit(0)

config_path = args.file
jobs_max = args.jobs
dump_json = args.dump
target = args.target

cli_vars = {}
if args.define:
  for pair in args.define:
    kv = pair.split('=')
    if len(kv) < 2:
      msg("variable needs '=': '{}'".format(pair), exit=1)
    cli_vars[kv[0]] = '='.join(kv[1:])


# --- load config

if not os.path.isfile(config_path):
  if config_default:
    usage()
  else:
    msg("file '{}' doesn't exist".format(config_path), exit=1)

with open(config_path, 'r') as f:
  def crash_out(e):
    msg("couldn't parse yaml for '{}':".format(config_path), e, exit=2)

  try:
    parsed = yaml.load(f, Loader=yaml.SafeLoader)
  except yaml.parser.ParserError as e:
    crash_out(e)
  except yaml.YAMLError as e:
    crash_out(e)

shell = parsed['shell'] if 'shell' in parsed else '/bin/bash'


targets = {}
config = {}

re_subs = re.compile(r'(\$[a-zA-Z0-9_]+)|(\$\(.+?\))')
def parse_sub(s):
  for match in re_subs.finditer(s):
    ms = match.group(0)
    if ms[1] == '(':
      cmd = ms[2:-1]
      result = subprocess.run(cmd, shell=True, executable=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
      if result.returncode:
        msg("shell substitution for '{}' failed ({}):".format(s, result.returncode), cmd)
        msg("output:", result.stdout, exit=3)
      val = result.stdout.strip() if result.stdout else ''
    else:
      key = ms[1:]
      val = config[key] if key in config else os.environ.get(key, '')
    s = s.replace(ms, val, 1)
  return s

if 'deps' not in parsed:
  msg("taiga.yml has no 'deps' hash", exit=4)
_deps = parsed['deps']

if 'targets' in parsed:
  for key in parsed['targets']:
    v = parsed['targets'][key]
    if isinstance(v, dict):
      l = {}
      for k in v:
        l[k] = parse_sub(v[k])
    else:
      l = []
      for dep in listify(v):
        l.append(parse_sub(dep))
    targets[parse_sub(key)] = l

if 'config' in parsed:
  config = parsed['config']
for key in config:
  config[key] = parse_sub(config[key])

for key in cli_vars:
  config[key] = parse_sub(cli_vars[key])


# --- check whether this target just specifies a command

if target and target in targets:
  this_target = targets[target]
  if isinstance(this_target, dict) and 'cmd' in this_target:
    cmd = this_target['cmd']
    print("--- {}".format(cmd))
    sys.stdout.flush()
    result = subprocess.run(cmd, shell=True, executable=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
    if result.returncode:
      msg("cmd failed ({}):".format(result.returncode), cmd)
      msg("output:", result.stdout, exit=5)
    if result.stdout:
      msg("output:", result.stdout)
    exit(0)


# --- create dict of every possible dependency's modification time

tree = {}
for root, dirs, files in os.walk('.'):
  root = root[2:] # remove ./
  if '.' in root:
    continue
  tree[root] = os.path.getmtime('./'+root)
  for f in files:
    this_path = os.path.join(root, f)
    tree[this_path] = os.path.getmtime(this_path)

def is_pattern(path):
  return '%' in path

def pattern_re(_path):
  parts = _path.split('%')
  return re.compile(r'^' + re.escape(parts[0]) + r'(.+)' + re.escape(parts[1]) + r'$')

def pattern_find(path, ignore=[]):

  def ignored(p):
    for ipath in ignore:
      if not is_pattern(ipath):
        if p == ipath:
          return True
      else:
        _r = pattern_re(ipath)
        if _r.match(p):
          return True
    return False

  def dir_expand(p):
    paths = [p]

    if os.path.isdir(p):
      p_slash = p + os.sep
      for k in tree:
        if k.startswith(p_slash):
          if not ignored(k):
            paths.append(k)

    return paths


  if not is_pattern(path):
    if ignored(path):
      return []
    return dir_expand(path)

  r = pattern_re(path)
  matches = []
  for k in tree:
    if k != path and r.match(k):
      if not ignored(k):
        matches += dir_expand(k)

  return matches

def pattern_swap(path, path_from, path_from_this):
  parts      = path.split('%')
  parts_from = path_from.split('%')
  r = re.compile(r'^' + re.escape(parts_from[0]) + r'(.+)' + re.escape(parts_from[1]) + r'$')
  return parts[0] + r.match(path_from_this).group(1) + parts[1]


changed = True
change_i = 0
path_patterns_previous = {}

def tree_mark(path):
  global tree
  global changed

  if is_pattern(path):
    msg("attempted to mark pattern '{}' in tree".format(path), exit=5)
  if path.startswith('..'):
    msg("attempted to mark file outside of build root: '{}'".format(path), exit=5)
  if path not in tree:
    tree[path] = True
    changed = True

while changed:
  changed = False
  change_i += 1

  for path in _deps:
    path = parse_sub(path)

    _dep = _deps[path]
    _from = listify(_dep['from'])
    _related = listify(_dep['related']) if 'related' in _dep else []
    _not = listify(_dep['not']) if 'not' in _dep else []

    if is_pattern(path):
      if (len(_from) != 1) or not is_pattern(_from[0]):
        msg("patterned dep '{}' expects a single patterned 'from'".format(path), exit=5)

      if path not in path_patterns_previous:
        path_patterns_previous[path] = {}

      for ppath in pattern_find(_from[0], _not):
        # prevent adding dependencies which match the target pattern
        if pattern_re(path).match(ppath):
          continue

        if ppath in path_patterns_previous[path]:
          continue
        tree_mark(ppath)
        path_patterns_previous[path][ppath] = True

        swapped = pattern_swap(path, _from[0], ppath)
        if swapped in path_patterns_previous[path]:
          continue
        tree_mark(swapped)
        path_patterns_previous[path][swapped] = True

    else:
      tree_mark(path)
      for fpath in _from:
        for ppath in pattern_find(fpath, _not):
          tree_mark(ppath)

    for rpath in _related:
      for ppath in pattern_find(rpath, _not):
        tree_mark(ppath)


# --- create dependency graph

def pattern_find_all(l, _not=[]):
  r = {}
  for path in l:
    for ppath in pattern_find(path, _not):
      r[ppath] = True
  rl = []
  for k in r:
    rl.append(k)
  return rl

deps = {}

for path in _deps:
  path = parse_sub(path)

  _dep = _deps[path]
  _from = listify(_dep['from'])
  _not = listify(_dep['not']) if 'not' in _dep else []

  def add_dep(apath, afrom):
    def remove_circular(_l):
      l = []
      for p in _l:
        if p != apath:
          l.append(p)
      return l

    deps[apath] = {
      'from':    remove_circular(afrom),
      'related': remove_circular(pattern_find_all(listify(_dep['related']), _not) if 'related' in _dep else []),
      'mkdir' :  _dep['mkdir'] if 'mkdir' in _dep else False,
      'cmd':     _dep['cmd'],
    }

  if is_pattern(path):
    for ppath in pattern_find(_from[0], _not):
      new_path = pattern_swap(path, _from[0], ppath)
      if ppath in path_patterns_previous[path] and new_path not in path_patterns_previous[path]:
        continue
      add_dep(new_path, [ppath])
  else:
    add_dep(path, pattern_find_all(_from, _not))


if dump_json:
  json_file = './taiga-deps.json'
  try:
    with open(json_file, 'w') as f:
      json.dump(deps, f, indent=2)
  except:
    msg("warning: couldn't write {}".format(json_file))


# --- build

loop = asyncio.get_event_loop()

builds = {}
build_cmds = {}
build_errs = {}

queue_ids = []
queue_tasks = {}

async def queue_wait(this_i):
  queue_ids.append(this_i)
  while len(queue_ids) > jobs_max:
    for queue_i in copy.copy(queue_ids):
      if queue_tasks[queue_i].done():
        queue_ids.remove(queue_i)
    for i, queue_i in enumerate(copy.copy(queue_ids)):
      if i < jobs_max and queue_i == this_i:
        return
    # nb. the below line *should* save time, but it doesn't seem to in tests (see #1).
    # await asyncio.wait(map(lambda x: queue_tasks[x], queue_ids), return_when=asyncio.FIRST_COMPLETED)
    await queue_tasks[queue_ids[0]]


def file_hash(path):
  if not os.path.exists(path):
    return None

  sha3 = hashlib.sha3_512()
  buf_size = 65536

  with open(path, 'rb') as f:
    while True:
      data = f.read(buf_size)
      if not data:
        break
      sha3.update(data)

  return sha3.hexdigest()

def touch(path):
  current_time = time.time()
  os.utime(path, (current_time, current_time))

def outdated(dep, depdep):
  return (tree[depdep] > tree[dep]) or (tree[dep] < 0)

build_i = 0
async def build(path, parent):
  global build_i

  if path not in tree:
    print("=== no rules to build non-existent dependency '{}' of '{}'".format(path, parent or '<root>'))
    exit(5)
  if path not in deps:
    return

  dep = deps[path]

  run = False
  all_deps = dep['from'] + dep['related']
  for f in all_deps:
    if f in builds:
      continue
    builds[f] = loop.create_task(build(f, path))

  for f in all_deps:
    await builds[f]
    if f in build_errs:
      return
    if outdated(path, f):
      run = True

  if not run:
    # mark this file as up-to-date if it doesn't need to be rebuilt
    touch(path)

  else:
    if path in build_cmds:
      await build_cmds[path]
      return

    d = os.path.dirname(path)
    if dep['mkdir']:
      os.makedirs(d, mode=0o777, exist_ok=True)

    config['IN']      = ' '.join(dep['from'])
    config['RELATED'] = ' '.join(dep['related'])
    config['OUT']     = path
    cmd = parse_sub(dep['cmd'])

    async def run_build_cmd(path, cmd, i):
      await queue_wait(i)
      print("--- {}".format(path))
      sys.stdout.flush()

      hash_before = file_hash(path)

      result = await asyncio.create_subprocess_shell(cmd, executable=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
      (stdout_data, stderr_data) = await result.communicate()
      stdout_decoded = stdout_data.decode('utf-8')

      if result.returncode:
        msg("'{}' build cmd failed ({}):".format(path, result.returncode), cmd)
        msg("output:", stdout_decoded)
        build_errs[path] = True
        return

      if not os.path.exists(path):
        msg("'{}' build cmd failed to create file".format(path), cmd)
        msg("'{}' output:".format(path), stdout_decoded)
        build_errs[path] = True
        return

      if stdout_decoded:
        msg("'{}' output:".format(path), stdout_decoded)

      if file_hash(path) != hash_before:
        # if hash has changed, mark file as updated in our in-memory tree
        tree[path] = time.time()
      else:
        # otherwise, touch this file to prevent dependencies rebuilding on
        # subsequent runs
        touch(path)

    build_cmds[path] = loop.create_task(run_build_cmd(path, cmd, build_i))
    queue_tasks[build_i] = build_cmds[path]
    build_i += 1
    await build_cmds[path]


async def root_build():
  global target

  if target:
    if target in targets:
      target_all_paths = pattern_find_all(targets[target])
    else:
      target_all_paths = pattern_find(target)

  root_builds = []
  for path in deps:
    if target and path not in target_all_paths:
      continue
    root_builds.append(loop.create_task(build(path, None)))

  if not root_builds:
    msg("nothing matched", exit=6)

  await asyncio.gather(*root_builds)

loop.run_until_complete(root_build())
loop.close()

if build_errs:
  msg("failed: {} total build errors".format(len(build_errs)))
  exit(5)
