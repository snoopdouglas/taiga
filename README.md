# taiga

> ⚠️ **Archived** – use [Scons](https://scons.org/) or something.

An incremental build tool that wants you to shut up and feed it noodles.

---

taiga takes a `taiga.yml` config, which at first glance is relatively similar to a makefile. However, targets and wildcards are simplified:

```yaml
deps:
  dist/ramen:
    from: src/o/%.o
    mkdir: true
    cmd: gcc $IN -o $OUT

  src/o/%.o:
    from: src/c/%.c
    related: src/c/%.h
    mkdir: true
    cmd: gcc -c $IN -o $OUT
```

Shown here is a pretty standard C build: `dist/ramen` is built from the object files in `src/o`. Unlike `make`, though:

* Object files' names are computed from the C sources in `src/c` without having to use `patsubst` and `wildcard`.
* Subdirectories within `src/c` are automatically recursed.
* We clearly differentiate between each object file's **direct** and **indirect** dependencies, using the `related` key.
* The `mkdir` switch does a `mkdir -p` on dependencies' dirnames, just to brighten up your day a bit.

## Usage

taiga runs on Python 3.6+, so install that first.

Then from your build root, run:

```
taiga
```

### Options

* `-f` / `--file` loads config from the given file rather than `./taiga.yml`.
* `-t` / `--target` builds a specific file or target (see below).
* `-j` / `--jobs` sets the maximum number of concurrent jobs. This defaults to your CPU count.
* `-d` / `--dump` dumps the full dependency mapping (as taiga sees it) to the given file, in JSON format.
* `-D` / `--define` can be used to provide a set variables that override values in `vars` (see below).
* `-v` / `--verbose` will enable verbose output (at some point in the future...)
* `-V` / `--version` prints taiga's version.

## Reference

```yaml
vars:
  CC: gcc
  CFLAGS: -Wall -O2
  LDFLAGS: $(pkg-config somelib --libs --cflags) -lm

targets:
  assets: dist/assets.tgz
  clean:
    cmd: rm -rf dist src/assets/tmp

deps:
  dist/ramen:
    from: src/o/%.o
    mkdir: true
    cmd: $CC $CFLAGS $IN -o $OUT $LDFLAGS
  dist/assets.tgz:
    from: src/assets
    not: src/assets/tmp
    mkdir: true
    cmd: cd src/assets; tar cvfz ../../$OUT $IN

  src/o/%.o:
    from: src/c/%.c
    related:
      - src/c/%.h
      - src/c/%.inl
    mkdir: true
    cmd: $CC $CFLAGS -c $IN -o $OUT
```

### Targets & dependencies

taiga always attempts to build every file listed in `deps` unless you've specified a target with `-t` / `--target`.

* `from`, `related` and `not` may each be a single path, or a list of paths.
* For the commands specified in each `cmd`, some variables are set:
  * `$IN` is `from`, space-seperated.
  * `$RELATED` is `related`, space-seperated.
  * `$OUT` is the file being built.
* Every file matching any path in `not` is excluded in both `from` and `related`.

There are a few gotchas here:

* One-to-one wildcard targets (like `src/o/%.o -> src/c/%.c` above) may only have one `from` dependency. Put the rest in `related`.
* Every single dependency is calculated before anything is built; any other files that might be by-products of your commands will be ignored for that build.
* If a file's content hasn't changed after it is rebuilt, its dependencies won't be rebuilt either; their modification times will just be updated in-place (`touch`).
* Dependencies that could also match the target pattern are ignored. This is such that eg.:
  * Say we have a target for `%.y.z -> %.z`, and a file `x.z` exists.
  * On the initial build, `x.y.z` will be built.
  * On subsequent builds, `x.y.y.z` *isn't* built.

Arbitrary targets can be specified with the `targets` hash - like `assets` above. These can refer to:

* A specific file or list of files. The `%` wildcard can be used in either case.
* Use `cmd` (like `clean` above) to run some command instead of building anything.

### Paths and patterns

* taiga's wildcard, `%`, is recursive; it expands in a similar manner to glob `**/*`.
* All paths specified in your config are relative to the build root (ie. the cwd when invoking `taiga`).
* taiga won't touch anything outside of the build root (ie. the cwd when you run `taiga`).
* If a path refers to an existing directory, one of two things will happen:
  * If the path is a target in `deps`, that directory's modification time will be used when calculating what needs to be rebuilt.
  * Otherwise, it will be expanded to all known files in that directory.

### Variables

Variables beginning with `$` are substituted, given the following precedence:

* Automatic variables: `IN`, `OUT`, `RELATED`
* Variables set via the CLI with `-D` / `--define`
* The `vars` hash
* A subshell, using `$(...)`
* The environment

Variables in the `vars` hash can themselves reference the environment or a subshell.

## License

MIT

> Copyright (c) 2019 Douglas Thompson
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
